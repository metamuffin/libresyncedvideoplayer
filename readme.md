# libresyncedvideoplayer

A libre synced video player.

Maybe my hosted version of this project is still availible on [lsvp.metamuffin.org](https://lsvp.metamuffin.org/)

## Installation

1. Install Deno
2. Clone this repo
3. `deno run --allow-read --allow-net --allow-env --unstable source/server/index.ts`

## Usage

Rooms are created implicitly and on-demand whenever you join them. Share your session with others via the URL.

### Environment variables

- `PORT` sets the port to listen on
- `HOST` sets the hostname to bind to

### Keyboard shortcuts

- `a` opens a prompt to change the video's source URL

## Todo

I would be happy about contributions. If you feel like it, consider implementing one of these:

- Ensure video is always in sync
- Speed playback up if you are behind the others
- Add keybind `f` to pay respect

## Licence

libresyncedvideoplayer - a libre synced video player
Copyright (C) 2022 metamuffin

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, version 3 of the
License only.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

See `LICENCE`