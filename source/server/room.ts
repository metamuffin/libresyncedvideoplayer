import { Router } from "https://deno.land/x/oak@v10.4.0/router.ts";
import { applyPacketToState, defaultState, filterPacketByState, Packet, PacketWrapper, State } from "../interface.ts";

export const api = new Router()

interface RoomState {
    clients: Map<string, WebSocket>
    state: State
}
const rooms = new Map<string, RoomState>()
const defaultRoomState = (): RoomState => ({ clients: new Map(), state: defaultState() })

api.get("/:room", async c => {
    await c.send({ path: "player.html", root: `${Deno.cwd()}/public` })
})

api.get("/:room/events", c => {
    const username = c.request.url.searchParams.get("username") ?? `unnamed#${Math.random()}`
    const room = c.params.room
    const ws = c.upgrade()

    const r = rooms.get(room) ?? defaultRoomState()
    rooms.set(room, r)

    const wrap_packet = (p: Packet): PacketWrapper => ({ origin: { user: username }, packet: p })
    const send_packet = (p: Packet) => ws.send(JSON.stringify(wrap_packet(p)))
    const broadcast_packet = (p: Packet) => r.clients.forEach(c => c.send(JSON.stringify(wrap_packet(p))))

    ws.onopen = () => {
        if (r.clients.has(username)) ws.close()
        r.clients.set(username, ws)
        broadcast_packet({ user_join: { username } })
    }
    ws.onmessage = ev => {
        let p: Packet = {}
        try { p = JSON.parse(ev.data.toString()) }
        catch (_) { ws.close() }

        if (p.noop) return // drop ping
        if (p.handshake_client) return send_packet({ handshake_server: { state: r.state } })
        if (!filterPacketByState(r.state, p)) return console.warn(`packet pruned: ${JSON.stringify(r.state)}`)
        applyPacketToState(r.state, p)
        broadcast_packet(p)
    }
    ws.onclose = () => {
        r.clients.delete(username)
        broadcast_packet({ user_leave: { username } })
        if (r.clients.size == 0) rooms.delete(room)
    }
})
