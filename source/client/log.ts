/// <reference lib="dom" />

import { defaultPresence, Packet, PacketWrapper, Presence } from "../interface.ts";


const logger = document.createElement("div")
logger.style.position = "absolute"
logger.style.top = "0px"
logger.style.left = "0px"
logger.classList.add("overlay-container")
document.body.appendChild(logger)

const presenceContainer = document.createElement("div")
presenceContainer.style.position = "absolute"
presenceContainer.style.top = "0px"
presenceContainer.style.right = "0px"
presenceContainer.classList.add("overlay-container")
document.body.appendChild(presenceContainer)

const remotePresence = new Map<string, { p: Presence, e?: HTMLDivElement }>();

export function loggerOnPacket({ packet, origin }: PacketWrapper) {
    const s = formatPacket(packet)
    if (s) {
        const mesg = document.createElement("p")
        mesg.classList.add("log")
        mesg.textContent = `${origin.user ?? "(server)"}: ${s}`
        logger.append(mesg)
        setTimeout(() => {
            mesg.remove()
        }, 5000)
    }
    if (packet.presence && origin.user) {
        const e = remotePresence.get(origin.user)?.e ?? createPresenceElement(origin.user)
        const p = packet.presence
        remotePresence.set(origin.user, { e, p })
        updatePresence(origin.user)
    }
    if (packet.user_leave && origin.user) {
        remotePresence.get(origin.user)?.e?.remove()
        remotePresence.delete(origin.user)
    }

}

function createPresenceElement(user: string): HTMLDivElement {
    const e = document.createElement("div")
    e.classList.add("presence")
    remotePresence.set(user, { p: defaultPresence(), e })
    presenceContainer.append(e)
    return e;
}

function updatePresence(user: string): void {
    const { p, e } = remotePresence.get(user)!
    if (!e) throw new Error("asdkl");
    e.textContent = `${user}: ${p.state}`

    if (p.ready) e.classList.add("p-ready")
    else e.classList.remove("p-ready")
}

function formatPacket(p: Packet): string | undefined {
    if (p.pause) return `paused at ${formatPosition(p.pause.time)}`
    if (p.resume) return `resumed at ${formatPosition(p.resume.time)}`
    if (p.seek) return `seeked to ${formatPosition(p.seek.time)}`
    if (p.url_change) return `changed video url to ${encodeURIComponent(p.url_change.url)}`
    if (p.user_join) return `joined this room`
    if (p.user_leave) return `left this room`
    return undefined
}

function formatPosition(p: number): string {
    return `${p.toFixed(0)}s`
}
