/// <reference lib="dom" />

import { applyPacketToState, defaultPresence, defaultState, filterPacketByState, Packet, PacketWrapper, Presence, State } from "../interface.ts"
import { loggerOnPacket } from "./log.ts";

let ws: WebSocket
const roomId = window.location.pathname.substring(1)
let state: State = defaultState()
const presence: Presence = defaultPresence()

export function startPlayer(username: string) {
    ws = new WebSocket(`ws${window.location.protocol.endsWith("s:") ? "s" : ""}://${window.location.host}/${encodeURIComponent(roomId)}/events?username=${encodeURIComponent(username)}`)
    ws.onopen = () => {
        console.log("websocket connected");
        send_packet({ handshake_client: true })
        setInterval(() => send_packet({ noop: true }), 30000) // TODO crude fix for stupid nginx disconnecting inactive connections
    }
    ws.onmessage = d => {
        // TODO queue up packets while the server state request is pending
        const p: PacketWrapper = JSON.parse(d.data)
        if (p.packet.handshake_server) {
            state = p.packet.handshake_server.state
            initPlayer()
        }
    }
    ws.onclose = () => { alert("you got disconnected"); window.location.reload() }
}

function send_packet(packet: Packet) {
    if (!filterPacketByState(state, packet)) return
    ws.send(JSON.stringify(packet))
}
function send_presence() { send_packet({ presence }) }

function initPlayer() {
    const player = document.createElement("video")
    player.controls = true
    player.classList.add("player")
    document.body.style.overflow = "hidden"

    player.src = state.url

    document.body.addEventListener("keypress", ev => {
        if (ev.code == "KeyA") {
            const url = prompt("paste url here")
            if (!url) return
            send_packet({ url_change: { url } })
            send_packet({ seek: { time: 0 } })
        }
    })

    ws.onmessage = d => {
        const { packet, origin }: PacketWrapper = JSON.parse(d.data)
        if (!filterPacketByState(state, packet)) return console.warn("packet dropped", packet)
        applyPacketToState(state, packet)
        loggerOnPacket({ packet, origin })
        if (packet.pause) { player.currentTime = packet.pause.time; player.pause() }
        if (packet.resume) { player.currentTime = packet.resume.time; player.play() }
        if (packet.seek) { player.currentTime = packet.seek.time }
        if (packet.url_change) { player.src = packet.url_change.url }
    }

    player.onseeked = () => {
        presence.state = state.paused ? "paused" : "playing"
        presence.ready = true
        send_presence()
        send_packet({ seek: { time: player.currentTime } })
    }
    player.onpause = () => {
        presence.state = "paused";
        send_presence()
        send_packet({ pause: { time: player.currentTime } })
    }
    player.onplay = () => {
        presence.state = "playing";
        send_presence()
        if (state.position != player.currentTime) player.currentTime = state.position
        send_packet({ resume: { time: player.currentTime } })
    }
    player.onwaiting = () => {
        presence.ready = false
        presence.state = "waiting"
        send_presence()
    }
    player.onstalled = () => {
        presence.ready = false
        presence.error = "stalled"
        send_presence()
    }
    player.oncanplaythrough = () => {
        delete presence.error
        presence.ready = true;
        send_presence()
    }
    player.onseeking = () => {
        presence.state = "seeking";
        send_presence()
    }

    ws.onclose = () => {
        console.error("ws closed");
    }

    document.body.append(player)
}
