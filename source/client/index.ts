/// <reference lib="dom" />

import { startPlayer } from "./player.ts";


window.onload = () => {

    const nameInput = document.createElement("input")
    nameInput.type = "text"
    nameInput.placeholder = "username"
    nameInput.value =  window.localStorage.getItem("username") ?? ""
    document.body.append(nameInput)

    const joinButton = document.createElement("input")
    joinButton.type = "button"
    joinButton.value = "join"
    joinButton.onclick = () => {
        window.localStorage.setItem("username", nameInput.value)
        startPlayer(nameInput.value)
        nameInput.remove()
        joinButton.remove()
    }
    document.body.append(joinButton)

}

