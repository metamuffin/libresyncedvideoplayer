
export interface PacketWrapper {
    packet: Packet
    origin: {
        user?: string,
        server?: boolean
    }
}

export interface Presence {
    state: "playing" | "seeking" | "paused" | "waiting"
    error?: string
    ready: boolean
}

export interface Packet {
    user_join?: { username: string }
    user_leave?: { username: string }
    presence?: Presence

    seek?: { time: number }
    pause?: { time: number }
    resume?: { time: number }

    url_change?: { url: string }

    handshake_client?: true
    handshake_server?: { state: State },

    noop?: true
}

export interface State {
    position: number
    paused: boolean
    url: string
}

export const defaultState = (): State => ({ paused: true, position: 0, url: "https://s.metamuffin.org/media/fallback.mp4" })
export const defaultPresence = (): Presence => ({ state: "waiting", ready: false })


export function applyPacketToState(s: State, p: Packet) {
    if (p.pause) s.paused = true, s.position = p.pause.time
    if (p.resume) s.paused = false, s.position = p.resume.time
    if (p.url_change) s.url = p.url_change.url
    if (p.seek) s.position = p.seek.time
}

export function filterPacketByState(s: State, p: Packet): boolean {
    if (p.pause && s.paused) delete p.pause
    if (p.resume && !s.paused) delete p.resume
    if (p.seek && p.seek.time == s.position) delete p.seek
    if (p.url_change && p.url_change.url == s.url) delete p.url_change
    return JSON.stringify(p) != "{}"
}
