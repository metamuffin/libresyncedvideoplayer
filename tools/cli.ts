import { parse } from "https://deno.land/std/flags/mod.ts";
import { Packet, PacketWrapper } from "../source/interface.ts";


const args = parse(Deno.args, {
    alias: {
        r: "room",
        h: "host",
        u: "url",
        U: "username"
    },
    boolean: ["secure"],
    default: {
        secure: true,
        host: "lsvp.metamuffin.org",
    },
})

if (!args.url) throw new Error("ples url");
if (!args.room) throw new Error("ples room");

const ws = new WebSocket(`ws${args.secure ? "s" : ""}://${args.host}/${encodeURIComponent(args.room)}/events${args.username ? `?username=${args.username}` : ""}`)

const send_packet = (p: Packet) => ws.send(JSON.stringify(p))
ws.onopen = () => {
    setInterval(() => send_packet({ noop: true }), 1000) // TODO crude fix for stupid nginx disconnecting inactive connections
    send_packet({ handshake_client: true })
}
ws.onmessage = ev => {
    const { packet, origin }: PacketWrapper = JSON.parse(ev.data)
    console.log(origin, packet);
    if (packet.handshake_server) {
        // if (!args.url) throw new Error("ples url");
        // send_packet({ url_change: { url: args.url } })
        // send_packet({ seek: { time: 0 } })
    }
    if (packet.url_change) {
        ws.close()
    }
}
ws.onclose = () => {
    Deno.exit(0)
}
